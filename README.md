**WeGo Supply Side Common Services**
*Reed Gauthier, Kathryn Reck, Sergiy Ensary, Summer Klimko, Kyle Flett*

**About**
This repository contains the files used to make the supply side common services pages. 
This is what a WeGo fleet manager/employee will use to sign in to their WeGo employee account or to create a new employee account to access the employee authorized pages. 


**How to use this repository**
Start at index.html to use the employee login page

**Next Steps**
The next steps for this repository will be to finish work on the supply side database tables to allow successful login/registration.