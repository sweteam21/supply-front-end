// Program Description: A user fills out the login form, the login info is then put in a dictionary,
//                      the dictionary is turned into a JSON readable string, the string is then passed t0
//                      runHTTPRequest.py
// Pre: The user has filled out the login form and pressed the submit button
// Post: A response is recieved from runHTTPRequest.py
function submitForm()
{
    var loginInfo = {"flag": "login", "username": document.getElementById('user').value, "password": document.getElementById('pass').value};


    strLoginInfo = JSON.stringify(loginInfo);


    var xhttp = new XMLHttpRequest();
    xhttp.open("POST", "https://demand.team21.softwareengineeringii.com/api/backend", true);
    xhttp.setRequestHeader("Content-type", "application/json");

    xhttp.onreadystatechange = function()
    {

        if(this.readyState == 4 && this.status == 200)
        {
            window.location="/comingSoon.html";
        }


    };

    xhttp.send(strLoginInfo);


}